#include <iostream>

#include "gen/abstractstubserver.h"
#include "relationaldatabase/query.h"
#include <jsonrpccpp/server/connectors/httpserver.h>
#include <stdio.h>

using namespace jsonrpc;
using namespace std;

class MyStubServer : public AbstractStubServer {
public:
    MyStubServer(AbstractServerConnector &connector, serverVersion_t type) : AbstractStubServer(connector, type) {};
    virtual std::string open__(const std::string& dbName, int maxStrLen, int pageSizeKB);
    virtual std::string create__(const Json::Value& paramsList, const std::string& tableName);
    virtual std::string drop__(const std::string& tableName);
    virtual std::string insert__(const Json::Value& paramsList, const std::string& tableName);
    virtual std::string update__(const Json::Value& paramsList, const std::string& tableName);
    virtual std::string delete__(const std::string& columnName, const std::string& columnType, const std::string& tableName, const std::string& value);
    virtual std::string selectAll__(const std::string& tableName);
    virtual std::string select__(const std::string& columnName, const std::string& columnType, const std::string& tableName, const std::string& value);
    virtual std::string join__(const std::string& firstCName, const std::string& firstTName, const std::string& secondCName, const std::string& secondTName);
    virtual std::string continue__();
    virtual void close__() {
        db_close(this->db);
        this->db = NULL;
    }
private:
    database* db = NULL;
    row_iterator* last_iter = NULL;
};

string MyStubServer::open__(const std::string &dbName, int maxStrLen, int pageSizeKB) {
    db = db_init();
    if (dbName.empty()) throw JsonRpcException(-1, "Database name was empty");
    if (maxStrLen <= 0 || pageSizeKB <= 0) throw JsonRpcException(-1, "Negative value in size(length)");
    enum result res = db_open(this->db, dbName.c_str(), pageSizeKB, maxStrLen);
    if (res != OK) {
        free(this->db);
        throw JsonRpcException(res, result_strings[res]);
    }
    return result_strings[res];
}

string MyStubServer::create__(const Json::Value &paramsList, const std::string &tableName) {
    if (!paramsList.isArray()) throw JsonRpcException(-1, "No list of arguments provided");
    if (tableName.empty()) throw JsonRpcException(-1, "Table name was empty");
    query* q = query_init_blank();
    q->type = CREATE_TABLE;
    q->db = this->db;
    q->first_table_name = tableName.c_str();
    data* dt = init_data(paramsList.size() / 2);
    result res;
    for (int i = 0; i < paramsList.size() / 2; i++) {
        if (paramsList[i*2].asString() == "STR") {
            res = init_str_col_data(dt, paramsList.size() / 2 - i - 1, paramsList[i*2 + 1].asCString(), "");
        } else if (paramsList[i*2].asString() == "INT") {
            res = init_int32_col_data(dt, paramsList.size() / 2 - i - 1, paramsList[i*2 + 1].asCString(), 0);
        } else if (paramsList[i*2].asString() == "DOUBLE") {
            res = init_double_col_data(dt, paramsList.size() / 2 - i - 1, paramsList[i*2 + 1].asCString(), 0.0);
        } else if (paramsList[i*2].asString() == "BOOL") {
            res = init_bool_col_data(dt, paramsList.size() / 2 - i - 1, paramsList[i*2 + 1].asCString(), false);
        } else {
            throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
        }
        if (res != OK) {
            close_query(q);
            close_data(dt);
            throw JsonRpcException(res, result_strings[res]);
        }
    }
    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        close_query(q);
        close_data(dt);
        throw JsonRpcException(res, result_strings[res]);
    }
    close_query(q);
    close_data(dt);
    return result_strings[res];
}

std::string MyStubServer::drop__(const std::string &tableName) {
    if (tableName.empty()) throw JsonRpcException(-1, "Table name was empty");
    query* q = query_init_blank();
    q->type = DROP_TABLE;
    q->db = this->db;
    q->first_table_name = tableName.c_str();
    result res = execute_query(q, NULL);
    if (res != OK) {
        close_query(q);
        throw JsonRpcException(res, result_strings[res]);
    }
    close_query(q);
    return result_strings[res];
}

std::string MyStubServer::insert__(const Json::Value &paramsList, const std::string &tableName) {
    if (!paramsList.isArray()) throw JsonRpcException(-1, "No list of arguments provided");
    if (tableName.empty()) throw JsonRpcException(-1, "Table name was empty");
    query* q = query_init_blank();
    q->type = INSERT;
    q->db = this->db;
    q->first_table_name = tableName.c_str();
    data* dt = init_data(paramsList.size() / 3);
    result res;
    for (int i = 0; i < paramsList.size() / 3; i++) {
        if (paramsList[i*3].asString() == "STR") {
            res = init_str_col_data(dt, paramsList.size() / 3 - i - 1, paramsList[i*3 + 1].asCString(), paramsList[i*3 + 2].asCString());
        } else if (paramsList[i*3].asString() == "INT") {
            res = init_int32_col_data(dt, paramsList.size() / 3 - i - 1, paramsList[i*3 + 1].asCString(), paramsList[i*3 + 2].asInt());
        } else if (paramsList[i*3].asString() == "DOUBLE") {
            res = init_double_col_data(dt, paramsList.size() / 3 - i - 1, paramsList[i*3 + 1].asCString(), paramsList[i*3 + 2].asDouble());
        } else if (paramsList[i*3].asString() == "BOOL") {
            res = init_bool_col_data(dt, paramsList.size() / 3 - i - 1, paramsList[i*3 + 1].asCString(), paramsList[i*3 + 2].asBool());
        } else {
            throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
        }
        if (res != OK) {
            close_query(q);
            close_data(dt);
            throw JsonRpcException(res, result_strings[res]);
        }
    }
    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        close_query(q);
        close_data(dt);
        throw JsonRpcException(res, result_strings[res]);
    }
    close_query(q);
    close_data(dt);
    return result_strings[res];
}

std::string MyStubServer::update__(const Json::Value &paramsList, const std::string &tableName) {
    if (!paramsList.isArray()) throw JsonRpcException(-1, "No list of arguments provided");
    if (tableName.empty()) throw JsonRpcException(-1, "Table name was empty");
    query* q = query_init_blank();
    q->type = UPDATE;
    q->db = this->db;
    q->first_table_name = tableName.c_str();
    q->col_name_first = paramsList[1].asCString();
    if (paramsList[0].asString() == "STR") {
        q->value = (void *) paramsList[2].asCString();
        q->col_type = VARCHAR;
    } else if (paramsList[0].asString() == "INT") {
        int32_t val = paramsList[2].asInt();
        q->value = (void *) (&val);
        q->col_type = INT32;
    } else if (paramsList[0].asString() == "DOUBLE") {
        double val = paramsList[2].asDouble();
        q->value = (void *) (&val);
        q->col_type = DOUBLE;
    } else if (paramsList[0].asString() == "BOOL") {
        bool val = paramsList[2].asBool();
        q->value = (void *) (&val);
        q->col_type = BOOLEAN;
    } else {
        throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
    }
    data* dt = init_data(paramsList.size() / 3 - 1);
    result res;
    for (int i = 1; i < paramsList.size() / 3; i++) {
        if (paramsList[i*3].asString() == "STR") {
            res = init_str_col_data(dt, paramsList.size() / 3 - i - 1, paramsList[i*3 + 1].asCString(), paramsList[i*3 + 2].asCString());
        } else if (paramsList[i*3].asString() == "INT") {
            res = init_int32_col_data(dt, paramsList.size() / 3 - i - 1, paramsList[i*3 + 1].asCString(), paramsList[i*3 + 2].asInt());
        } else if (paramsList[i*3].asString() == "DOUBLE") {
            res = init_double_col_data(dt, paramsList.size() / 3 - i - 1, paramsList[i*3 + 1].asCString(), paramsList[i*3 + 2].asDouble());
        } else if (paramsList[i*3].asString() == "BOOL") {
            res = init_bool_col_data(dt, paramsList.size() / 3 - i - 1, paramsList[i*3 + 1].asCString(), paramsList[i*3 + 2].asBool());
        } else {
            throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
        }
        if (res != OK) {
            close_query(q);
            close_data(dt);
            throw JsonRpcException(res, result_strings[res]);
        }
    }
    q->dt = dt;
    res = execute_query(q, NULL);
    if (res != OK) {
        close_query(q);
        close_data(dt);
        throw JsonRpcException(res, result_strings[res]);
    }
    close_query(q);
    close_data(dt);
    return result_strings[res];
}

std::string
MyStubServer::delete__(const std::string &columnName, const std::string &columnType, const std::string &tableName,
                       const std::string &value) {
    if (tableName.empty()) throw JsonRpcException(-1, "Table name was empty");
    if (columnName.empty()) throw JsonRpcException(-1, "Column name was empty");
    if (columnType.empty()) throw JsonRpcException(-1, "Column type was empty");
    if (value.empty()) throw JsonRpcException(-1, "Value was empty");
    query* q = query_init_blank();
    q->type = DELETE;
    q->db = this->db;
    q->first_table_name = tableName.c_str();
    q->col_name_first = columnName.c_str();
    if (columnType == "STR") {
        q->value = (void *) value.c_str();
        q->col_type = VARCHAR;
    } else if (columnType == "INT") {
        int32_t val = atoi(value.c_str());
        q->value = (void *) (&val);
        q->col_type = INT32;
    } else if (columnType == "DOUBLE") {
        double val = atof(value.c_str());
        q->value = (void *) (&val);
        q->col_type = DOUBLE;
    } else if (columnType == "BOOL") {
        bool val = value == "true";
        q->value = (void *) (&val);
        q->col_type = BOOLEAN;
    } else {
        throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
    }

    result res = execute_query(q, NULL);
    if (res != OK) {
        close_query(q);
        throw JsonRpcException(res, result_strings[res]);
    }
    close_query(q);
    return result_strings[res];
}

std::string MyStubServer::selectAll__(const std::string &tableName) {
    if (tableName.empty()) throw JsonRpcException(-1, "Table name was empty");
    query* q = query_init_blank();
    q->type = SELECT_ALL;
    q->db = this->db;
    q->first_table_name = tableName.c_str();
    close_iterator(this->last_iter, this->db);
    this->last_iter = iter_init();
    result res = execute_query(q, this->last_iter);
    if (res != OK) {
        close_iterator(this->last_iter, this->db);
        this->last_iter = NULL;
        close_query(q);
        throw JsonRpcException(res, result_strings[res]);
    }
    int counter = 0;
    string result;
    while (iter_valid(this->last_iter) && counter < 10) {
        data* dt = extract_iterator(this->last_iter);
        if (counter == 0) {
            for (int i = 0; i < dt->col_num; i++) {
                result += dt->columns[i].name;
                result += "    ";
            }
            result += '\n';
        }
        for (int i = 0; i < dt->col_num; i++) {
            switch (dt->columns[i].header->col_header) {
                case INT32:
                    result += to_string((*((int32_t *) dt->data[i]))) + "    ";
                    break;
                case VARCHAR:
                    result += (char *) dt->data[i];
                    result += "    ";
                    break;
                case DOUBLE:
                    result += to_string((*((double *) dt->data[i]))) + "    ";
                    break;
                case BOOLEAN:
                    result += *((bool *) dt->data[i]) ? "true    " : "false    ";
                    break;
            }
        }
        result += '\n';
        counter++;
        table_next(this->last_iter);
    }
    if (counter == 10 && iter_valid(this->last_iter)) {
        result += "To load more - use CONTINUE\n";
    }
    if (!iter_valid(this->last_iter)) {
        close_iterator(this->last_iter, this->db);
        this->last_iter = NULL;
    }
    return result;
}

std::string
MyStubServer::select__(const std::string &columnName, const std::string &columnType, const std::string &tableName,
                       const std::string &value) {
    if (tableName.empty()) throw JsonRpcException(-1, "Table name was empty");
    if (columnName.empty()) throw JsonRpcException(-1, "Column name was empty");
    if (columnType.empty()) throw JsonRpcException(-1, "Column type was empty");
    if (value.empty()) throw JsonRpcException(-1, "Value was empty");
    query* q = query_init_blank();
    q->type = SELECT;
    q->db = this->db;
    q->first_table_name = tableName.c_str();
    q->col_name_first = columnName.c_str();
    if (columnType == "STR") {
        q->value = (void *) value.c_str();
        q->col_type = VARCHAR;
    } else if (columnType == "INT") {
        int32_t val = atoi(value.c_str());
        q->value = (void *) (&val);
        q->col_type = INT32;
    } else if (columnType == "DOUBLE") {
        double val = atof(value.c_str());
        q->value = (void *) (&val);
        q->col_type = DOUBLE;
    } else if (columnType == "BOOL") {
        bool val = value == "true";
        q->value = (void *) (&val);
        q->col_type = BOOLEAN;
    } else {
        throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
    }
    close_iterator(this->last_iter, this->db);
    this->last_iter = iter_init();
    result res = execute_query(q, this->last_iter);
    if (res != OK) {
        close_iterator(this->last_iter, this->db);
        this->last_iter = NULL;
        close_query(q);
        throw JsonRpcException(res, result_strings[res]);
    }
    int counter = 0;
    string result;
    while (iter_valid(this->last_iter) && counter < 10) {
        data* dt = extract_iterator(this->last_iter);
        if (counter == 0) {
            for (int i = 0; i < dt->col_num; i++) {
                result += dt->columns[i].name;
                result += "    ";
            }
            result += '\n';
        }
        for (int i = 0; i < dt->col_num; i++) {
            switch (dt->columns[i].header->col_header) {
                case INT32:
                    result += to_string((*((int32_t *) dt->data[i]))) + "    ";
                    break;
                case VARCHAR:
                    result += (char *) dt->data[i];
                    result += "    ";
                    break;
                case DOUBLE:
                    result += to_string((*((double *) dt->data[i]))) + "    ";
                    break;
                case BOOLEAN:
                    result += *((bool *) dt->data[i]) ? "true    " : "false    ";
                    break;
            }
        }
        result += '\n';
        counter++;
        table_next(this->last_iter);
    }
    if (counter == 10 && iter_valid(this->last_iter)) {
        result += "To load more - use CONTINUE\n";
    }
    if (!iter_valid(this->last_iter)) {
        close_iterator(this->last_iter, this->db);
        this->last_iter = NULL;
    }
    return result;
}

std::string
MyStubServer::join__(const std::string &firstCName, const std::string &firstTName, const std::string &secondCName,
                     const std::string &secondTName) {
    if (firstCName.empty()) throw JsonRpcException(-1, "First column name was empty");
    if (firstTName.empty()) throw JsonRpcException(-1, "Firts table name was empty");
    if (secondCName.empty()) throw JsonRpcException(-1, "Second column name was empty");
    if (secondTName.empty()) throw JsonRpcException(-1, "Second table name was empty");
    query* q = query_init_blank();
    q->type = JOIN;
    q->db = this->db;
    q->first_table_name = firstTName.c_str();
    q->col_name_first = firstCName.c_str();
    q->second_table_name = secondTName.c_str();
    q->col_name_second = secondCName.c_str();
    close_iterator(this->last_iter, this->db);
    this->last_iter = iter_init();
    result res = execute_query(q, this->last_iter);
    if (res != OK) {
        close_iterator(this->last_iter, this->db);
        this->last_iter = NULL;
        close_query(q);
        throw JsonRpcException(res, result_strings[res]);
    }
    int counter = 0;
    string result;
    while (iter_valid(this->last_iter) && counter < 10) {
        data* dt = extract_iterator(this->last_iter);
        if (counter == 0) {
            for (int i = 0; i < dt->col_num; i++) {
                result += dt->columns[i].name;
                result += "    ";
            }
            result += '\n';
        }
        for (int i = 0; i < dt->col_num; i++) {
            switch (dt->columns[i].header->col_header) {
                case INT32:
                    result += to_string((*((int32_t *) dt->data[i]))) + "    ";
                    break;
                case VARCHAR:
                    result += (char *) dt->data[i];
                    result += "    ";
                    break;
                case DOUBLE:
                    result += to_string((*((double *) dt->data[i]))) + "    ";
                    break;
                case BOOLEAN:
                    result += *((bool *) dt->data[i]) ? "true    " : "false    ";
                    break;
            }
        }
        result += '\n';
        counter++;
        table_next(this->last_iter);
    }
    if (counter == 10 && iter_valid(this->last_iter)) {
        result += "To load more - use CONTINUE\n";
    }
    if (!iter_valid(this->last_iter)) {
        close_iterator(this->last_iter, this->db);
        this->last_iter = NULL;
    }
    return result;
}

std::string MyStubServer::continue__() {
    if (this->last_iter == NULL) return "Nothing to load anymore";
    int counter = 0;
    string result;
    while (iter_valid(this->last_iter) && counter < 10) {
        data* dt = extract_iterator(this->last_iter);
        if (counter == 0) {
            for (int i = 0; i < dt->col_num; i++) {
                result += dt->columns[i].name;
                result += "    ";
            }
            result += '\n';
        }
        for (int i = 0; i < dt->col_num; i++) {
            switch (dt->columns[i].header->col_header) {
                case INT32:
                    result += to_string((*((int32_t *) dt->data[i]))) + "    ";
                    break;
                case VARCHAR:
                    result += (char *) dt->data[i];
                    result += "    ";
                    break;
                case DOUBLE:
                    result += to_string((*((double *) dt->data[i]))) + "    ";
                    break;
                case BOOLEAN:
                    result += *((bool *) dt->data[i]) ? "true    " : "false    ";
                    break;
            }
        }
        result += '\n';
        counter++;
        table_next(this->last_iter);
    }
    if (counter == 10 && iter_valid(this->last_iter)) {
        result += "To load more - use CONTINUE\n";
    }
    if (!iter_valid(this->last_iter)) {
        close_iterator(this->last_iter, this->db);
        this->last_iter = NULL;
    }
    return result;
}

int main(int argc, char *argv[]) {
    if (argc < 5) {
        cout << "Usage: ./sampleserver `port` `db_file_name` `max_str_len` `page_size_kb`" << endl;
        return 1;
    }
    int port = atoi(argv[1]);
    int max_str_len = atoi(argv[3]);
    int page_size_kb = atoi(argv[4]);
    HttpServer httpserver(port);
    MyStubServer s(httpserver,
                   JSONRPC_SERVER_V2);
    s.StartListening();
    try {
        s.open__(argv[2], max_str_len, page_size_kb);
    } catch (JsonRpcException e) {
        cout << e.what() << endl;
        s.StopListening();
        return 1;
    }
    cout << "Hit enter to stop the server" << endl;
    getchar();
    s.close__();
    s.StopListening();
}