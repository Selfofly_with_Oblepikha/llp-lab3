#ifndef RELATIONALDATABASE_DATA_H
#define RELATIONALDATABASE_DATA_H

#include "utils.h"

enum columnType {
    INT32,
    VARCHAR,
    DOUBLE,
    BOOLEAN
};

typedef struct data data;
typedef struct columnHeader columnHeader;
typedef struct column column;

struct columnHeader {
    enum columnType col_header;
    uint32_t col_size;
    uint32_t col_name_len;
};

struct column {
    columnHeader* header;
    char* name;
};

struct data {
    uint32_t col_num;
    column* columns;
    void** data;
};

data* init_data(uint32_t col_n);
void close_data(data* data);
enum result init_int32_col_data(data* data, uint32_t col_n, const char * col_name, int32_t value);
enum result init_double_col_data(data* data, uint32_t col_n, const char * col_name, double value);
enum result init_bool_col_data(data* data, uint32_t col_n, const char * col_name, bool value);
enum result init_str_col_data(data* data, uint32_t col_n, const char * col_name, const char * value);

#endif //RELATIONALDATABASE_DATA_H
