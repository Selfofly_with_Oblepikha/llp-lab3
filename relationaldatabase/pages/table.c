#include "table.h"

void table_begin(tableHeaderPage* table, row_iterator* iter) {
    iter->row_n = 0;
    if (table->header->first_data_page_offset != 0) {
        dataPage* dp = (dataPage*) malloc(sizeof(dataPage));
        read_data(dp, table->file_descriptor, table->header->first_data_page_offset, table);
        iter->page = dp;
    } else {
        iter->page = NULL;
    }
}

void table_end(tableHeaderPage* table, row_iterator* iter) {
    if (table->header->first_data_page_offset == 0) {
        table_begin(table, iter);
        return;
    }
    iter->page = (dataPage*) malloc(sizeof(dataPage));
    read_data(iter->page, table->file_descriptor, table->header->last_data_page_offset, table);
    iter->row_n = iter->page->header->rows_num;
}

enum result table_next(row_iterator* iter) {
    if (iter->page == NULL || iter->row_n == iter->page->header->rows_num) {
        printf("Error addressing next element: no such element exists");
        return NO_SUCH_ELEMENT;
    }
    enum result res;
    iter->row_n++;
    if (iter->row_n == iter->page->header->rows_num && iter->page->header->next_page_offset == 0) {
        data_page_write(iter->page->table->file_descriptor, iter->page);
        tableHeaderPage* table = iter->page->table;
        close_data_page(iter->page);
        table_end(table, iter);
        return OK;
    }
    if (iter->row_n >= iter->page->header->rows_num && iter->page->header->next_page_offset == 0) {
        printf("Error addressing next element - iterator invalidated!");
        return NO_SUCH_ELEMENT;
    }
    if (iter->row_n >= iter->page->header->rows_num) {
        iter->row_n = 0;
        uint32_t off = iter->page->header->next_page_offset;
        data_page_write(iter->page->table->file_descriptor, iter->page);
        tableHeaderPage* table = iter->page->table;
        close_data_page(iter->page);
        iter->page = (dataPage*) malloc(sizeof(dataPage));
        res = read_data(iter->page, table->file_descriptor, off, table);
        if (res != OK) {
            printf("In read_data()\n");
            return res;
        }
    }
    return OK;
}

enum result table_prev(row_iterator* iterator) {
    if (iterator->row_n == 0 && iterator->page->header->page_num == 1) {
        return NO_SUCH_ELEMENT;
    }
    enum result res;
    if (iterator->row_n == 0) {
        uint32_t off = iterator->page->header->prev_page_offset;
        data_page_write(iterator->page->table->file_descriptor, iterator->page);
        tableHeaderPage* table = iterator->page->table;
        close_data_page(iterator->page);
        iterator->page = (dataPage*) malloc(sizeof(dataPage));
        res = read_data(iterator->page, table->file_descriptor, off, table);
        if (res != OK) {
            printf("In read_data()\n");
            return res;
        }
        iterator->row_n = iterator->page->header->rows_num - 1;
    } else {
        iterator->row_n--;
    }
    return OK;
}


data* extract_iterator(row_iterator* iterator) {
    data* dt = init_data(iterator->page->table->header->columns_num);
    for (int i = 0; i < iterator->page->table->header->columns_num; i++) {
        switch (iterator->page->table->columns[i].header->col_header) {
            case VARCHAR:
                init_str_col_data(dt, i,
                                  iterator->page->table->columns[i].name,
                                  ((char*) iterator->page->data[iterator->row_n][i]));
                break;
            case INT32:
                init_int32_col_data(dt, i,
                                    iterator->page->table->columns[i].name,
                                    *((int32_t*) iterator->page->data[iterator->row_n][i]));
                break;
            case DOUBLE:
                init_double_col_data(dt, i,
                                     iterator->page->table->columns[i].name,
                                     *((double*) iterator->page->data[iterator->row_n][i]));
                break;
            case BOOLEAN:
                init_bool_col_data(dt, i,
                                     iterator->page->table->columns[i].name,
                                     *((bool*) iterator->page->data[iterator->row_n][i]));
                break;
        }
    }
    return dt;
}

enum result init_basic_col_table(tableHeaderPage* page, uint32_t col_n, const char * col_name) {
    if (col_n > page->header->columns_num) {
        printf("Error addressing column: not enough columns");
        return ERROR_ADDRESSING_COLUMN;
    }
    page->columns[col_n].header = (columnHeader*) malloc(sizeof(columnHeader));
    int len = 1;
    while (col_name[len - 1] != '\0') len++;
    page->columns[col_n].header->col_name_len = len;
    page->columns[col_n].name = (char*) malloc(len);
    for (int i = 0; i < len; i++) {
        page->columns[col_n].name[i] = col_name[i];
    }
    page->columns[col_n].name[len - 1] = '\0';
    return OK;
}

enum result init_int32_col_table(tableHeaderPage* page, uint32_t col_n, const char * col_name) {
    enum result res = init_basic_col_table(page, col_n, col_name);
    if (res != OK) {
        printf("In init_basic_col_table()");
        return res;
    }
    page->columns[col_n].header->col_header = INT32;
    page->columns[col_n].header->col_size = sizeof(int32_t);
    return OK;
}

enum result init_double_col_table(tableHeaderPage* page, uint32_t col_n, const char * col_name) {
    enum result res = init_basic_col_table(page, col_n, col_name);
    if (res != OK) {
        printf("In init_basic_col_table()");
        return res;
    }
    page->columns[col_n].header->col_header = DOUBLE;
    page->columns[col_n].header->col_size = sizeof(double);
    return OK;
}

enum result init_bool_col_table(tableHeaderPage* page, uint32_t col_n, const char * col_name) {
    enum result res = init_basic_col_table(page, col_n, col_name);
    if (res != OK) {
        printf("In init_basic_col_table()");
        return res;
    }
    page->columns[col_n].header->col_header = BOOLEAN;
    page->columns[col_n].header->col_size = sizeof(bool);
    return OK;
}

enum result init_str_col_table(tableHeaderPage* page, uint32_t col_n, const char * col_name) {
    uint32_t col_size = page->header->max_string_length;
    enum result res = init_basic_col_table(page, col_n, col_name);
    if (res != OK) {
        printf("In init_basic_col_table()");
        return res;
    }
    page->columns[col_n].header->col_header = VARCHAR;
    page->columns[col_n].header->col_size = col_size;
    return OK;
}



enum result read_data(dataPage* page, int fd, uint32_t off, tableHeaderPage* table) {
    off_t offset = lseek(fd, off, SEEK_SET);

    if (offset == -1) {
        printf("Error seeking: %d\n", errno);
        return ERROR_SEEKING;
    }
    page->header = (dataPageHeader *) malloc(sizeof(dataPageHeader));

    ssize_t bytes_read = read(fd, page->header, sizeof(dataPageHeader));
    if (bytes_read == -1) {
        printf("Error reading file: %d\n", errno);
        free(page->header);
        return ERROR_READING_FILE;
    }

    uint16_t row_len = 0;
    for (int i = 0; i < table->header->columns_num; i++) {
        row_len += table->columns[i].header->col_size;
    }

    uint16_t max_rows_num = (table->header->page_size_kilobytes * 1024 - sizeof(dataPageHeader)) / row_len;
    page->data = (void***) malloc(max_rows_num * sizeof(void**));
    for (int i = 0; i < max_rows_num; i++) {
        page->data[i] = (void**) malloc(table->header->columns_num * sizeof(void*));
        for (int j = 0; j < table->header->columns_num; j++) {
            page->data[i][j] = malloc(table->columns[j].header->col_size);
            bytes_read = read(fd, page->data[i][j], table->columns[j].header->col_size);
            if (bytes_read == -1) {
                printf("Error reading file: %d\n", errno);
                for (int k = 0; k <= j; k++) {
                    free(page->data[i][k]);
                }
                free(page->data[i]);
                return ERROR_READING_FILE;
            }
        }
    }

    page->table = table;
    return OK;
}

enum result read_table_header(tableHeader* header, int fd, uint32_t off) {
    off_t offset = lseek(fd, off, SEEK_SET);

    if (offset == -1) {
        printf("Error seeking: %d\n", errno);
        return ERROR_SEEKING;
    }

    ssize_t bytes_read = read(fd, header, sizeof(tableHeader));
    if (bytes_read == -1) {
        printf("Error reading file: %d\n", errno);
        return ERROR_READING_FILE;
    }
    return OK;
}

enum result read_table(tableHeaderPage* page, int fd, uint32_t off) {
    page->header = (tableHeader *) malloc(sizeof(tableHeader));
    enum result res = read_table_header(page->header, fd, off);
    if (res != OK) {
        printf("In read_table_header()");
        return res;
    }

    page->table_name = (char*) malloc(page->header->table_name_len);
    ssize_t bytes_read = read(fd, page->table_name, page->header->table_name_len);
    if (bytes_read == -1) {
        printf("Error reading file: %d\n", errno);
        free(page->table_name);
        free(page->header);
        return ERROR_READING_FILE;
    }

    page->columns = (column*) malloc(sizeof(column) * page->header->columns_num);

    if (page->header->initialized) {
        for (int i = 0; i < page->header->columns_num; i++) {
            page->columns[i].header = (columnHeader *) malloc(sizeof(columnHeader));
            bytes_read = read(fd, page->columns[i].header, sizeof(columnHeader));
            if (bytes_read == -1) {
                printf("Error reading file: %d\n", errno);
                for (int k = 0; k <= i; k++) {
                    if (k < i) free(page->columns[k].name);
                    free(page->columns[k].header);
                }
                free(page->columns);
                return ERROR_READING_FILE;
            }
            page->columns[i].name = (char*) malloc(page->columns[i].header->col_name_len);
            bytes_read = read(fd, page->columns[i].name, page->columns[i].header->col_name_len);
            if (bytes_read == -1) {
                printf("Error reading file: %d\n", errno);
                for (int k = 0; k <= i; k++) {
                    free(page->columns[k].header);
                    free(page->columns[k].name);
                }
                free(page->columns);
                return ERROR_READING_FILE;
            }
        }
    }
    page->is_virtual = false;
    page->file_descriptor = fd;
    return OK;
}

void close_data_page(dataPage* page) {
    if (page == NULL) return;

    uint16_t row_len = 0;
    for (int i = 0; i < page->table->header->columns_num; i++) {
        row_len += page->table->columns[i].header->col_size;
    }

    uint16_t max_rows_num = (page->table->header->page_size_kilobytes * 1024 - sizeof(dataPageHeader)) / row_len;

    free(page->header);
    for (int i = 0; i < max_rows_num; i++) {
        for (int j = 0; j < page->table->header->columns_num; j++) {
            free(page->data[i][j]);
        }
        free(page->data[i]);
    }
    free(page->data);
    free(page);
}

void close_table(tableHeaderPage* page) {
    if (page == NULL) return;
    if (page->header->table_name_len != 0) free(page->table_name);
    if (page->header->initialized) {
        for (int i = 0; i < page->header->columns_num; i++) {
            free(page->columns[i].name);
            free(page->columns[i].header);
        }
    }
    free(page->header);
    free(page->columns);
    free(page);
}

enum result data_page_write(int fd, dataPage* page) {
    if (page == NULL) return OK;

    off_t offset = lseek(fd, page->header->this_offset, SEEK_SET);

    if (offset == -1) {
        printf("Error seeking: %d\n", errno);
        return ERROR_SEEKING;
    }

    ssize_t bytes_written = write(fd, page->header, sizeof(dataPageHeader));
    if (bytes_written == -1) {
        printf("Error writing to file: %d\n", errno);
        return ERROR_WRITING_TO_FILE;
    }

    uint16_t row_len = 0;
    for (int i = 0; i < page->table->header->columns_num; i++) {
        row_len += page->table->columns[i].header->col_size;
    }

    uint16_t max_rows_num = (page->table->header->page_size_kilobytes * 1024 - sizeof(dataPageHeader)) / row_len;

    for (int i = 0; i < max_rows_num; i++) {
        for (int j = 0; j < page->table->header->columns_num; j++) {
            bytes_written = write(fd, page->data[i][j], page->table->columns[j].header->col_size);
            if (bytes_written == -1) {
                printf("Error writing to file: %d\n", errno);
                return ERROR_WRITING_TO_FILE;
            }
        }
    }
    return OK;
}

enum result write_table_header(int fd, tableHeader* header) {
    off_t offset = lseek(fd, header->this_offset, SEEK_SET);

    if (offset == -1) {
        printf("Error seeking: %d\n", errno);
        return ERROR_SEEKING;
    }
    ssize_t bytes_written = write(fd, header, sizeof(tableHeader));
    if (bytes_written == -1) {
        printf("Error writing to file: %d\n", errno);
        return ERROR_WRITING_TO_FILE;
    }
    return OK;
}

enum result write_table(int fd, tableHeaderPage* page) {
    if (page == NULL) return OK;

    enum result res = write_table_header(fd, page->header);
    if (res != OK) {
        printf("In write_table_header()");
        return res;
    }

    ssize_t bytes_written = write(fd, page->table_name, page->header->table_name_len);
    if (bytes_written == -1) {
        printf("Error writing to file: %d\n", errno);
        return ERROR_WRITING_TO_FILE;
    }
    if (page->header->initialized) {
        for (int i = 0; i < page->header->columns_num; i++) {
            bytes_written = write(fd, page->columns[i].header, sizeof(columnHeader));
            if (bytes_written == -1) {
                printf("Error writing to file: %d\n", errno);
                return ERROR_WRITING_TO_FILE;
            }
            bytes_written = write(fd, page->columns[i].name, page->columns[i].header->col_name_len);
            if (bytes_written == -1) {
                printf("Error writing to file: %d\n", errno);
                return ERROR_WRITING_TO_FILE;
            }
        }
    }

    return OK;
}

void data_page_init(tableHeaderPage* table, uint32_t offset) {
    dataPage* page = (dataPage*) malloc(sizeof(dataPage));
    page->header = (dataPageHeader*) malloc(sizeof(dataPageHeader));
    page->table = table;
    page->header->this_offset = offset;
    page->header->page_size_kilobytes = table->header->page_size_kilobytes;
    page->header->page_num = table->header->num_pages + 1;
    page->header->next_page_offset = 0;
    page->header->prev_page_offset = table->header->last_data_page_offset;
    page->header->free_spc_offset = offset + sizeof(dataPageHeader);
    page->header->rows_num = 0;
    uint16_t row_len = 0;
    for (int i = 0; i < table->header->columns_num; i++) {
        row_len += table->columns[i].header->col_size;
    }

    uint16_t max_rows_num = (table->header->page_size_kilobytes * 1024 - sizeof(dataPageHeader)) / row_len;
    page->data = (void***) malloc(max_rows_num * sizeof(void**));
    for (int i = 0; i < max_rows_num; i++) {
        page->data[i] = (void**) malloc(table->header->columns_num * sizeof(void*));
        for (int j = 0; j < table->header->columns_num; j++) {
            page->data[i][j] = malloc(table->columns[j].header->col_size);
        }
    }

    table->header->num_pages++;

    uint32_t prev_off = table->header->last_data_page_offset;
    table->header->last_data_page_offset = page->header->this_offset;

    if (table->header->num_pages == 1) {
        table->header->first_data_page_offset = page->header->this_offset;
        data_page_write(table->file_descriptor, page);
        close_data_page(page);
        return;
    }


    dataPage* next = (dataPage*) malloc(sizeof(dataPage));
    read_data(next, table->file_descriptor, prev_off, table);
    next->header->next_page_offset = page->header->this_offset;
    data_page_write(table->file_descriptor, next);
    close_data_page(next);

    data_page_write(table->file_descriptor, page);
    close_data_page(page);
}
