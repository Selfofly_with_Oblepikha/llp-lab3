#ifndef RELATIONALDATABASE_TABLE_H
#define RELATIONALDATABASE_TABLE_H

#include "../data.h"

typedef struct dataPageHeader dataPageHeader;
typedef struct dataPage dataPage;
typedef struct tableHeader tableHeader;
typedef struct tableHeaderPage tableHeaderPage;
typedef struct row_iterator row_iterator;

struct dataPageHeader {
    uint32_t this_offset;
    uint32_t page_num;
    uint32_t page_size_kilobytes;
    uint32_t free_spc_offset;
    uint32_t next_page_offset;
    uint32_t prev_page_offset;
    uint32_t rows_num;
};

struct dataPage {
    tableHeaderPage* table;
    dataPageHeader* header;
    void*** data;
};

struct tableHeader{
    uint32_t this_offset;
    uint32_t table_name_len;
    uint32_t columns_num;
    uint32_t page_size_kilobytes;
    uint32_t num_pages;
    uint32_t first_data_page_offset;
    uint32_t last_data_page_offset;
    uint32_t next_table_offset;
    uint32_t max_string_length;
    bool initialized;
};

struct tableHeaderPage{
    int file_descriptor;
    bool is_virtual;
    char* table_name;
    tableHeader* header;
    column* columns;
};

struct row_iterator {
    dataPage* page;
    int64_t row_n;
};

enum result init_int32_col_table(tableHeaderPage* page, uint32_t col_n, const char * col_name);
enum result init_double_col_table(tableHeaderPage* page, uint32_t col_n, const char * col_name);
enum result init_bool_col_table(tableHeaderPage* page, uint32_t col_n, const char * col_name);
enum result init_str_col_table(tableHeaderPage* page, uint32_t col_n, const char * col_name);

enum result read_data(dataPage* page, int fd, uint32_t off, tableHeaderPage* table);
enum result data_page_write(int fd, dataPage* page);

enum result read_table_header(tableHeader* header, int fd, uint32_t off);
enum result read_table(tableHeaderPage* page, int fd, uint32_t off);

void close_table(tableHeaderPage* page);
void close_data_page(dataPage* page);

enum result write_table_header(int fd, tableHeader* header);
enum result write_table(int fd, tableHeaderPage* page);

void data_page_init(tableHeaderPage* page, uint32_t offset);

void table_begin(tableHeaderPage* table, row_iterator* iter);
void table_end(tableHeaderPage* table, row_iterator* iter);
enum result table_next(row_iterator* iterator);
enum result table_prev(row_iterator* iterator);
data* extract_iterator(row_iterator* iterator);


#endif //RELATIONALDATABASE_TABLE_H
