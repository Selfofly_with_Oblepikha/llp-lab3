#include "query.h"
#include "pages/database.h"
#include "pages/table.h"

database* db_init() {
    database* db = (database*) malloc(sizeof(database));
    return db;
}

row_iterator* iter_init() {
    row_iterator* iter = (row_iterator*) malloc(sizeof(row_iterator));
    iter->page = NULL;
    iter->row_n = 0;
    return iter;
}

bool iter_valid(row_iterator* iterator) {
    return iterator->page != NULL && iterator->row_n != iterator->page->header->rows_num;
}

int get_fd(database* db) {
    return db->file_descriptor;
}


query* query_init_blank() {
    query* q = (query*) malloc(sizeof(query));
    q->type = SELECT_ALL;
    q->db = NULL;
    q->first_table_name = NULL;
    q->second_table_name = NULL;
    q->dt = NULL;
    q->col_name_first = NULL;
    q->col_type = INT32;
    q->value = NULL;
    q->col_name_second = NULL;
    return q;
}

void close_query(query* q) {
    if (q == NULL) return;
    free(q);
}

enum result execute_query(query* q, row_iterator* iterator) {
    enum result res;
    switch (q->type) {
        case CREATE_TABLE:
            if (q->db == NULL || q->first_table_name == NULL || q->dt == NULL) return NOT_ENOUGH_ARGUMENTS;
            write_table(q->db->file_descriptor, q->db->last_used_table);
            write_table(q->db->file_descriptor, q->db->join_used_table);
            close_table(q->db->last_used_table);
            close_table(q->db->join_used_table);
            q->db->last_used_table = NULL;
            q->db->join_used_table = NULL;

            res = new_table(q->db, q->first_table_name, q->dt->col_num);
            if (res != OK) {
                printf("In new_table()\n");
                return res;
            }

            res = open_table(q->db, &q->db->last_used_table, q->first_table_name);
            if (res != OK) {
                printf("In open_table()\n");
                return res;
            }

            for (int i = 0; i < q->dt->col_num; i++) {
                switch (q->dt->columns[i].header->col_header) {
                    case INT32:
                        res = init_int32_col_table(q->db->last_used_table, i, q->dt->columns[i].name);
                        if (res != OK) {
                            printf("In init_int32_col_table()");
                            return res;
                        }
                        break;
                    case DOUBLE:
                        res = init_double_col_table(q->db->last_used_table, i, q->dt->columns[i].name);
                        if (res != OK) {
                            printf("In init_double_col_table()");
                            return res;
                        }
                        break;
                    case BOOLEAN:
                        res = init_bool_col_table(q->db->last_used_table, i, q->dt->columns[i].name);
                        if (res != OK) {
                            printf("In init_bool_col_table()");
                            return res;
                        }
                        break;
                    case VARCHAR:
                        res = init_str_col_table(q->db->last_used_table, i, q->dt->columns[i].name);
                        if (res != OK) {
                            printf("In init_str_col_table()");
                            return res;
                        }
                        break;
                    default:
                        return UNKNOWN_COLUMN_TYPE;
                }
            }
            q->db->last_used_table->header->initialized = true;
            break;
        case DROP_TABLE:
            if (q->db == NULL || q->first_table_name == NULL) return NOT_ENOUGH_ARGUMENTS;
            res = open_table(q->db, &q->db->last_used_table, q->first_table_name);
            if (res != OK) {
                printf("In open_table()");
                return res;
            }
            res = remove_table(q->db, q->db->last_used_table);
            if (res != OK) {
                printf("In remove_table()");
                return res;
            }
            break;
        case INSERT:
            if (q->db == NULL || q->first_table_name == NULL || q->dt == NULL) return NOT_ENOUGH_ARGUMENTS;
            res = open_table(q->db, &q->db->last_used_table, q->first_table_name);
            if (res != OK) {
                printf("In open_table()");
                return res;
            }
            res = insert_data(q->db, q->db->last_used_table, q->dt);
            if (res != OK) {
                printf("In insert_data()");
                return res;
            }
            break;
        case UPDATE:
            if (q->db == NULL || q->first_table_name == NULL || q->dt == NULL || q->col_name_first == NULL || q->value == NULL) return NOT_ENOUGH_ARGUMENTS;
            res = open_table(q->db, &q->db->last_used_table, q->first_table_name);
            if (res != OK) {
                printf("In open_table()");
                return res;
            }
            res = update_data_where(q->db, q->db->last_used_table, q->dt, q->col_name_first, q->col_type, q->value);
            if (res != OK) {
                printf("In update_data_where()");
                return res;
            }
            break;
        case DELETE:
            if (q->db == NULL || q->first_table_name == NULL || q->col_name_first == NULL || q->value == NULL) return NOT_ENOUGH_ARGUMENTS;
            res = open_table(q->db, &q->db->last_used_table, q->first_table_name);
            if (res != OK) {
                printf("In open_table()");
                return res;
            }
            res = remove_where(q->db, q->db->last_used_table, q->col_name_first, q->col_type, q->value);
            if (res != OK) {
                printf("In remove_where()");
                return res;
            }
            break;
        case SELECT_ALL:

            if (q->db == NULL || q->first_table_name == NULL || iterator == NULL) return NOT_ENOUGH_ARGUMENTS;
            res = open_table(q->db, &q->db->last_used_table, q->first_table_name);
            if (res != OK) {
                printf("In open_table()");
                return res;
            }
            res = select_all(iterator, q->db, q->db->last_used_table);
            if (res != OK) {
                printf("In select_all()");
                return res;
            }
            break;
        case SELECT:
            if (q->db == NULL || q->first_table_name == NULL || q->col_name_first == NULL || q->value == NULL || iterator == NULL) return NOT_ENOUGH_ARGUMENTS;

            res = open_table(q->db, &q->db->last_used_table, q->first_table_name);
            if (res != OK) {
                printf("In open_table()");
                return res;
            }
            res = select_where(iterator, q->db, q->db->last_used_table, q->col_name_first, q->col_type, q->value);
            if (res != OK) {
                printf("In select_where()");
                return res;
            }
            break;
        case JOIN:
            if (q->db == NULL || q->first_table_name == NULL || q->col_name_first == NULL || q->second_table_name == NULL || q->col_name_second == NULL || iterator == NULL) return NOT_ENOUGH_ARGUMENTS;
            res = open_table(q->db, &q->db->last_used_table, q->first_table_name);
            if (res != OK) {
                printf("In open_table()");
                return res;
            }
            res = open_table(q->db, &q->db->join_used_table, q->second_table_name);
            if (res != OK) {
                printf("In open_join_table()");
                return res;
            }
            res = join_tables(iterator, q->db, q->db->last_used_table, q->col_name_first, q->db->join_used_table, q->col_name_second);
            if (res != OK) {
                printf("In join_tables()");
                return res;
            }
            break;
        default:
            return UNKNOWN_QUERY_TYPE;
    }
    return OK;
}


