#include <iostream>

#include "gen/stubclient.h"
#include <jsonrpccpp/client/connectors/httpclient.h>
#include "./sql-parser/parser.tab.hh"
#include "sql-parser/node.h"
#include "relationaldatabase/utils.h"

using namespace jsonrpc;
using namespace std;

int main(int argc, char *argv[]) {
    if (argc < 2) {
        cout << "Usage: ./sampleclient `port`" << endl;
        return 1;
    }
    string address = "http://localhost:";
    address += argv[1];
    HttpClient httpclient(address);
    StubClient c(httpclient, JSONRPC_CLIENT_V2);
    Node* node = NULL;
    while (true) {
        try {
            node = yyparse();
            if (node == NULL) throw JsonRpcException(-1, "Unknown error");
            if (node->type == SELECT_my) {
                if (node->third != NULL) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                if (node->first != NULL && node->second != NULL) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                string table_name = node->v_first.str;
                if (node->first == NULL && node->second == NULL) {
                    //selectAll
                    cout << c.selectAll__(table_name) << endl;
                } else if (node->first != NULL) {
                    //selectWhere
                    if (node->first->type != COMPARE_my) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                    if (node->first->v_first.comp != EQUAL_my) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                    if ((node->first->first->type == NAME_my && node->first->second->type == NAME_my) &&
                        (node->first->first->type != NAME_my && node->first->second->type != NAME_my))
                        throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                    Node* column = node->first->first->type == NAME_my ? node->first->first : node->first->second;
                    Node* term =  node->first->first->type == NAME_my ? node->first->second : node->first->first;
                    if (column->v_first.str != table_name) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                    string column_name = column->v_second.str;
                    string column_type;
                    string value;
                    switch (term->type) {
                        case INTEGER_my:
                            column_type = "INT";
                            value = to_string(term->v_first.integer);
                            break;
                        case FLOAT_my:
                            column_type = "DOUBLE";
                            value = to_string(term->v_first.flt);
                            break;
                        case BOOL_my:
                            column_type = "BOOL";
                            value = term->v_first.boolean ? "true" : "false";
                            break;
                        case STRING_my:
                            column_type = "STR";
                            value = term->v_first.str;
                            value = value.substr(1, value.length() - 2);
                            break;
                        default:
                            throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
                    }
                    cout << c.select__(column_name, column_type, table_name, value) << endl;
                } else {
                    //join
                    Node* cmp = node->second;
                    if (cmp->v_first.comp != EQUAL_my) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                    string firstTname = node->v_first.str;
                    string secondTname = node->v_second.str;
                    if (firstTname != cmp->first->v_first.str || secondTname != cmp->second->v_first.str) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                    cout << c.join__(cmp->first->v_second.str, cmp->first->v_first.str, cmp->second->v_second.str, cmp->second->v_first.str) << endl;
                }
            } else if (node->type == INSERT_my) {
                Node *curr = node->first;
                string table_name = node->v_first.str;
                Json::Value paramsList(Json::arrayValue);
                string value;
                while (curr != NULL) {
                    switch (curr->first->second->type) {
                        case INTEGER_my:
                            paramsList.append("INT");
                            paramsList.append(curr->first->v_first.str);
                            paramsList.append(curr->first->second->v_first.integer);
                            break;
                        case FLOAT_my:
                            paramsList.append("DOUBLE");
                            paramsList.append(curr->first->v_first.str);
                            paramsList.append(curr->first->second->v_first.flt);
                            break;
                        case BOOL_my:
                            paramsList.append("BOOL");
                            paramsList.append(curr->first->v_first.str);
                            paramsList.append(curr->first->second->v_first.boolean);
                            break;
                        case STRING_my:
                            paramsList.append("STR");
                            paramsList.append(curr->first->v_first.str);
                            value = curr->first->second->v_first.str;
                            value = value.substr(1, value.length() - 2);
                            paramsList.append(value);
                            break;
                        default:
                            throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
                    }
                    curr = curr->second;
                }
                cout << c.insert__(paramsList, table_name) << endl;
            } else if (node->type == DELETE_my) {
                if (node->first == NULL) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                string table_name = node->v_first.str;
                if (node->first->type != COMPARE_my) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                if (node->first->v_first.comp != EQUAL_my) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                if ((node->first->first->type == NAME_my && node->first->second->type == NAME_my) &&
                        (node->first->first->type != NAME_my && node->first->second->type != NAME_my))
                    throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                Node* column = node->first->first->type == NAME_my ? node->first->first : node->first->second;
                Node* term =  node->first->first->type == NAME_my ? node->first->second : node->first->first;
                if (column->v_first.str != table_name) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                string column_name = column->v_second.str;
                string column_type;
                string value;
                switch (term->type) {
                    case INTEGER_my:
                        column_type = "INT";
                        value = to_string(term->v_first.integer);
                        break;
                    case FLOAT_my:
                        column_type = "DOUBLE";
                        value = to_string(term->v_first.flt);
                        break;
                    case BOOL_my:
                        column_type = "BOOL";
                        value = term->v_first.boolean ? "true" : "false";
                        break;
                    case STRING_my:
                        column_type = "STR";
                        value = term->v_first.str;
                        value = value.substr(1, value.length() - 2);
                        break;
                    default:
                        throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
                }
                cout << c.delete__(column_name, column_type, table_name, value) << endl;
            } else if (node->type == UPDATE_my) {
                if (node->first == NULL) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                string table_name = node->v_first.str;
                if (node->first->type != COMPARE_my) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                if (node->first->v_first.comp != EQUAL_my) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                if ((node->first->first->type == NAME_my && node->first->second->type == NAME_my) &&
                    (node->first->first->type != NAME_my && node->first->second->type != NAME_my))
                    throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                Node* column = node->first->first->type == NAME_my ? node->first->first : node->first->second;
                Node* term =  node->first->first->type == NAME_my ? node->first->second : node->first->first;
                if (column->v_first.str != table_name) throw JsonRpcException(UNSUPPORTED_OPERATION, result_strings[UNSUPPORTED_OPERATION]);
                string value;
                Json::Value paramsList(Json::arrayValue);
                switch (term->type) {
                    case INTEGER_my:
                        paramsList.append("INT");
                        paramsList.append(column->v_second.str);
                        paramsList.append(term->v_first.integer);
                        break;
                    case FLOAT_my:
                        paramsList.append("DOUBLE");
                        paramsList.append(column->v_second.str);
                        paramsList.append(term->v_first.flt);
                        break;
                    case BOOL_my:
                        paramsList.append("BOOL");
                        paramsList.append(column->v_second.str);
                        paramsList.append(term->v_first.boolean);
                        break;
                    case STRING_my:
                        paramsList.append("STR");
                        paramsList.append(column->v_second.str);
                        value = term->v_first.str;
                        value = value.substr(1, value.length() - 2);
                        paramsList.append(value);
                        break;
                    default:
                        throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
                }
                Node* curr = node->second;
                while (curr != NULL) {
                    switch (curr->first->second->type) {
                        case INTEGER_my:
                            paramsList.append("INT");
                            paramsList.append(curr->first->v_first.str);
                            paramsList.append(curr->first->second->v_first.integer);
                            break;
                        case FLOAT_my:
                            paramsList.append("DOUBLE");
                            paramsList.append(curr->first->v_first.str);
                            paramsList.append(curr->first->second->v_first.flt);
                            break;
                        case BOOL_my:
                            paramsList.append("BOOL");
                            paramsList.append(curr->first->v_first.str);
                            paramsList.append(curr->first->second->v_first.boolean);
                            break;
                        case STRING_my:
                            paramsList.append("STR");
                            paramsList.append(curr->first->v_first.str);
                            value = curr->first->second->v_first.str;
                            value = value.substr(1, value.length() - 2);
                            paramsList.append(value);
                            break;
                        default:
                            throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
                    }
                    curr = curr->second;
                }
                cout << c.update__(paramsList, table_name) << endl;
            } else if (node->type == CREATE_my) {
                Node *curr = node->first;
                string table_name = node->v_first.str;
                Json::Value paramsList(Json::arrayValue);
                while (curr != NULL) {
                    switch (curr->first->second->v_first.data_type) {
                        case INT_my:
                            paramsList.append("INT");
                            break;
                        case FLT_my:
                            paramsList.append("DOUBLE");
                            break;
                        case BOOLEAN_my:
                            paramsList.append("BOOL");
                            break;
                        case STR_my:
                            paramsList.append("STR");
                            break;
                        default:
                            throw JsonRpcException(UNKNOWN_COLUMN_TYPE, result_strings[UNKNOWN_COLUMN_TYPE]);
                    }
                    paramsList.append(curr->first->v_first.str);
                    curr = curr->second;
                }
                cout << c.create__(paramsList, table_name) << endl;
            } else if (node->type == DROP_my) {
                cout << c.drop__(node->v_first.str) << endl;
            } else if (node->type == CONTINUE_my) {
                cout << c.continue__() << endl;
            } else {
                throw JsonRpcException(UNKNOWN_QUERY_TYPE, result_strings[UNKNOWN_QUERY_TYPE]);
            }
        } catch (JsonRpcException &e) {
            cerr << e.what() << endl;
        }
    }

}