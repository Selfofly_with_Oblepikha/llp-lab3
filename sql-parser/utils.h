#pragma once
#include <malloc.h>
#include "node.h"

char* str_copy (const char* from);
void str_concat(char** str, const char * str2);

char* to_string(Node* node, int tabs);
