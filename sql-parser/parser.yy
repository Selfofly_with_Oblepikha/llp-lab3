%code requires{
    #include "node.h"
    #define YYERROR_VERBOSE 1

    extern int yylex();
    extern int yylineno;
    void yyerror(const char*);
}

%define parse.error verbose

%token SELECT INSERT UPDATE CREATE DELETE DROP CONTINUE FROM SET INTO WHERE JOIN VALUES ON OTHER

%token
    END 0       "end of file"

    OPEN_CIRCLE_BRACKET         "("
    OPEN_FIGURE_BRACKET         "{"
    CLOSE_CIRCLE_BRACKET        ")"
    CLOSE_FIGURE_BRACKET        "}"
    ENDLINE     ";"
    COLON     ":"
    COMMA       ","
    DOT         "."
    AND         "and"
    OR          "or"
    EQ          "="
    STAR        "*"
;
%token TYPE

%token CMP BOOL INT FLOAT STR QSTR

%type <intval> CMP
%type <intval> TYPE
%type <boolval> BOOL
%type <intval> INT
%type <floatval> FLOAT
%type <str> STR
%type <str> QSTR
%type <intval> AND
%type <intval> OR

// You declare the types of nonterminals using %type.
%type <nonterminal> root
%type <nonterminal> query

%type <nonterminal> select_query
%type <nonterminal> empty_select
%type <nonterminal> where_select
%type <nonterminal> join_empty_select
%type <nonterminal> join_where_select

%type <nonterminal> delete_query
%type <nonterminal> empty_delete
%type <nonterminal> where_delete

%type <nonterminal> update_query
%type <nonterminal> empty_update
%type <nonterminal> where_update

%type <nonterminal> create_query
%type <nonterminal> drop_query

%type <nonterminal> where_statement
%type <nonterminal> logic_statement
%type <nonterminal> on_statement
%type <nonterminal> column
%type <nonterminal> terminal

%type <nonterminal> insert_query
%type <nonterminal> values_list
%type <nonterminal> values_pair
%type <nonterminal> variables_list
%type <nonterminal> variable
%type <nonterminal> columns_list

%type <nonterminal> continue


%union {
    char str[50];
    int intval;
    bool boolval;
    Node* nonterminal;
    float floatval;

    int cmp_type;
    int logic_op;
    int type;
}

%%

root:
|   root query ENDLINE { print_tree($2); printf("$> "); }
;

query: select_query
|   insert_query
|   delete_query
|   drop_query
|   update_query
|   create_query
|   continue
;

select_query:
|   empty_select
|   where_select
|   join_empty_select
|   join_where_select
;
empty_select:
|   SELECT STAR FROM STR { $$ = new_select($4, NULL, NULL, NULL, NULL); }
|   SELECT columns_list FROM STR { $$ = new_select($4, NULL, NULL, NULL, $2); }
;
where_select:
|   SELECT STAR FROM STR WHERE where_statement { $$ = new_select($4, $6, NULL, NULL, NULL); }
|   SELECT columns_list FROM STR WHERE where_statement { $$ = new_select($4, $6, NULL, NULL, $2); }
;

join_empty_select:
|   SELECT STAR FROM STR JOIN STR ON on_statement { $$ = new_select($4, NULL, $6, $8, NULL); }
|   SELECT columns_list FROM STR JOIN STR ON on_statement { $$ = new_select($4, NULL, $6, $8, $2); }
;

join_where_select:
|   SELECT STAR FROM STR JOIN STR ON on_statement WHERE where_statement { $$ = new_select($4, $10, $6, $8, NULL); }
|   SELECT columns_list FROM STR JOIN STR ON on_statement WHERE where_statement { $$ = new_select($4, $10, $6, $8, $2); }
;

update_query:
|   empty_update
|   where_update
;
empty_update:
|   UPDATE STR SET "(" values_list ")" { $$ = new_update($2, NULL, $5); }
;
where_update:
|   UPDATE STR SET "(" values_list ")" WHERE where_statement { $$ = new_update($2, $8, $5); }
;

delete_query:
|   empty_delete
|   where_delete
;
empty_delete:
|   DELETE FROM STR { $$ = new_delete($3, NULL); }
;
where_delete:
|   DELETE FROM STR WHERE where_statement { $$ = new_delete($3, $5);}
;

insert_query:
|   INSERT INTO STR VALUES "(" values_list ")" { $$ = new_insert($3, $6); }
;

create_query:
|   CREATE STR "(" variables_list ")" { $$ = new_create($2, $4); }
;

drop_query:
|   DROP STR { $$ = new_drop($2); }
;

continue:
|   CONTINUE { $$ = new_continue(); }
;

values_list:
|   values_list "," values_pair { $$ = new_list($3, $1); }
|   values_pair { $$ = new_list($1, NULL); }
;

values_pair:
|   STR "=" terminal { $$ = new_pair($1, $3); }
;

variables_list:
|   variables_list "," variable { $$ = new_list($3, $1); }
|   variable { $$ = new_list($1, NULL); }
;

variable:
|   STR terminal { $$ = new_pair($1, $2); }
;

columns_list:
|   columns_list "," column { $$ = new_list($3, $1); }
|   column { $$ = new_list($1, NULL); }
;

where_statement:
|   where_statement "and" where_statement { $$ = new_where($2, $1, $3); }
|   where_statement "or" where_statement { $$ = new_where($2, $1, $3); }
|   "(" where_statement ")" { $$ = $2; }
|   logic_statement
;

on_statement:
|   column CMP column { $$ = new_compare($2, $1, $3); }
;

logic_statement:
|   column CMP terminal { $$ = new_compare($2, $1, $3); }
|   terminal CMP column { $$ = new_compare(reverse_Comparison($2), $1, $3); }
|   column CMP column { $$ = new_compare($2, $1, $3); }
;

column:
|   STR DOT STR { $$ = new_name($1, $3); }
;

terminal:
|   TYPE { $$ = new_type($1); }
|   INT { $$ = new_integer($1); }
|   FLOAT { $$ = new_float($1); }
|   BOOL { $$ = new_bool($1); }
|   QSTR { $$ = new_string($1); }
;

%left "+" "-";
%left "*" "/" "%";

%left OR;
%left AND;

%%

void yyerror (const char *s) {
   fprintf (stderr, "%s on line number %d", s, yylineno);
}



