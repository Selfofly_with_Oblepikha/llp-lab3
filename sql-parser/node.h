#pragma once
#include <stdbool.h>

enum NodeType {
    NAME_my,
    STRING_my,
    INTEGER_my,
    FLOAT_my,
    BOOL_my,
    LIST_my,
    PAIR_my,
    SELECT_my,
    INSERT_my,
    DELETE_my,
    UPDATE_my,
    CREATE_my,
    DROP_my,
    WHERE_my,
    COMPARE_my,
    TYPE_my,
    CONTINUE_my
};

enum DataType {
    STR_my,
    INT_my,
    FLT_my,
    BOOLEAN_my
};

static const char* DataType_strings[] = {
        "STR",
        "INT",
        "FLT",
        "BOOLEAN"
};

enum LogicOperation {
    AND_my,
    OR_my
};

static const char* LogicOperation_strings[] = {
        "AND",
        "OR"
};

enum Comparison {
    CONTAINS_my,
    GREATER_my,
    GREATER_OR_EQUAL_my,
    EQUAL_my,
    NO_COMPARE_my,
    NOT_EQUAL_my,
    LESS_OR_EQUAL_my,
    LESS_my,
    SUBSTR_my
};

int reverse_Comparison(int val);

static const char* Comparison_strings[] = {
	"CONTAINS",
        "GREATER",
        "GREATER_OR_EQUAL",
        "EQUAL",
        "NO_COMPARE",
        "NOT_EQUAL",
        "LESS_OR_EQUAL",
        "LESS",
        "IS SUBSTR OF"
};

union Value {
    char* str;
    enum LogicOperation log_op;
    enum Comparison comp;
    int integer;
    float flt;
    bool boolean;
    enum DataType data_type;
};

typedef struct Node Node;
typedef union Value Value;
typedef enum NodeType NodeType;

struct Node {
    NodeType type;
    Value v_first;
    Value v_second;
    Node* first;
    Node* second;
    Node* third;
};

Node* new_name(const char* v_first, const char* v_second);
Node* new_string(const char* v_first);
Node* new_integer(int v_first);
Node* new_float(float v_first);
Node* new_bool(bool v_first);
Node* new_type(int v_first);
Node* new_list(Node* first, Node* second);
Node* new_pair(const char* v_first, Node* second);
Node* new_select(const char* v_first, Node* first, const char* v_second, Node* second, Node* third);
Node* new_delete(const char* v_first, Node* first);
Node* new_insert(const char* v_first, Node* first);
Node* new_update(const char* v_first, Node* first, Node* second);
Node* new_create(const char* v_first, Node* first);
Node* new_drop(const char* v_first);
Node* new_where(int v_first, Node* first, Node* second);
Node* new_compare(int v_first, Node* first, Node* second);
Node* new_continue();

void close_tree(Node* root);

